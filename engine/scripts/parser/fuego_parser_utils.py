#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2017 Toshiba corp.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""
fuego_parser_utils.py - This library is contains common independent functions
By Daniel Sangorrin (August 2017)
"""

# helper function for printing warnings and errors
def hls(string,type_char):
    if type_char == "w":
        type_string = "WARNING"
    else:
        type_string = "ERROR"

    print "########################### " + type_string + " ###############################"
    print string
    print "-----"

def split_test_id(test_case_id):
    if "." in test_case_id:
        parts = test_case_id.split('.')
        test_set_name = ".".join(parts[:-1])
        test_case_name = parts[-1]
    else:
        test_set_name = "default"
        test_case_name = test_case_id

    return test_set_name, test_case_name

def get_test_case(test_case_id, run_data):
    test_set_name, test_case_name = split_test_id(test_case_id)

    for test_set in run_data['test_sets']:
        if test_set['name'] == test_set_name:
            for test_case in test_set['test_cases']:
                if test_case['name'] == test_case_name:
                    return test_case

    hls("Unable to get test case " + test_case_id, "e")
    return None

