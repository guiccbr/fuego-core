# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


# DESCRIPTION
# This script contains report generating functionality: creating logrun files, creating test results files and producing PDF output


. $FUEGO_CORE/engine/scripts/common.sh

REP_LOGGEN="$FUEGO_CORE/engine/scripts/loggen/loggen.py"
REP_DIR="$FUEGO_RW/logs/logruns/"
OF_ROOT="$FUEGO_CORE/engine/overlays/"
REP_GEN="$FUEGO_CORE/engine/scripts/loggen/gentexml.py"

LOGDIR="$FUEGO_RW/logs/${TESTDIR}/${NODE_NAME}.${TESTSPEC}.${BUILD_NUMBER}.${BUILD_ID}"
TEST_RES="$LOGDIR/res.json"
GEN_TESTRES_FILE=""

function check_create_logrun () {

    if [ "$BATCH_TESTPLAN" ] && [ "$LOGRUN_FILE" ]
    then
        REP_LOGRUN_FILE=$REP_DIR/${NODE_NAME}.$LOGRUN_FILE

        if [ ! -e $REP_LOGRUN_FILE ]
        then
            echo "creating $REP_LOGRUN_FILE logrun file"

            run_python $REP_LOGGEN --create-logrun $REP_LOGRUN_FILE --testplan $BATCH_TESTPLAN --board ${NODE_NAME} --run-num $BUILD_NUMBER
        fi

        run_python $REP_LOGGEN --logrun-file $REP_LOGRUN_FILE --append-logfile $TEST_RES  --testname ${JOB_NAME}
    fi
}

function check_create_functional_logrun () {

    if [ "$BATCH_TESTPLAN" ] && [ "$LOGRUN_FILE" ]
    then
        REP_LOGRUN_FILE=$REP_DIR/${NODE_NAME}.$LOGRUN_FILE

        if [ ! -e $REP_LOGRUN_FILE ]
        then
            echo "creating $REP_LOGRUN_FILE logrun file"

            run_python $REP_LOGGEN --create-logrun $REP_LOGRUN_FILE --testplan $BATCH_TESTPLAN --board ${NODE_NAME} --run-num $BUILD_NUMBER
        fi

        run_python $REP_LOGGEN --append-funcres "$1" --logrun-file $REP_LOGRUN_FILE --testname ${JOB_NAME}
    fi
}

# Set GEN_TESTRES_FILE.  If this variable is set, the parser will output
# the test metrics in json format for later report generation.
function set_testres_file () {
    GEN_TESTRES_FILE=$TEST_RES
}

function gen_report() {
    if [ "$BATCH_TESTPLAN" ] && [ "$LOGRUN_FILE" ]
    then
        REP_LOGRUN_FILE=$REP_DIR/${NODE_NAME}.$LOGRUN_FILE
        REPORT_XML=$REP_LOGRUN_FILE.xml

        run_python $REP_GEN --logrun-file $REP_LOGRUN_FILE --output-file $REPORT_XML
        texml $REPORT_XML $REPORT_XML.tex

        mkdir -p pdf_reports

        cd pdf_reports
        cp $REPORT_XML.tex .

        pdflatex $REPORT_XML.tex

    else
        echo "Omitting report generation"
    fi
}
