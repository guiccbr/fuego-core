tarball=dbench-3.04.tar.gz

function test_build {
    patch -N -s -p1 < $TEST_HOME/dbench_startup.patch
    ./configure --host=$HOST --build=`uname -m`-linux-gnu  CFLAGS="$CFLAGS";
    make
}

function test_deploy {
    put dbench client.txt $BOARD_TESTDIR/fuego.$TESTDIR/ 
}

function test_run {
    assert_define BENCHMARK_DBENCH_MOUNT_BLOCKDEV
    assert_define BENCHMARK_DBENCH_MOUNT_POINT
    assert_define BENCHMARK_DBENCH_TIMELIMIT
    assert_define BENCHMARK_DBENCH_NPROCS
    
    hd_test_mount_prepare $BENCHMARK_DBENCH_MOUNT_BLOCKDEV $BENCHMARK_DBENCH_MOUNT_POINT
        
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; cp client.txt $BENCHMARK_DBENCH_MOUNT_POINT/fuego.$TESTDIR; pwd; ./dbench -t $BENCHMARK_DBENCH_TIMELIMIT -D $BENCHMARK_DBENCH_MOUNT_POINT/fuego.$TESTDIR -c $BENCHMARK_DBENCH_MOUNT_POINT/fuego.$TESTDIR/client.txt $BENCHMARK_DBENCH_NPROCS; rm $BENCHMARK_DBENCH_MOUNT_POINT/fuego.$TESTDIR/client.txt"
    
    sleep 5
        
    hd_test_clean_umount $BENCHMARK_DBENCH_MOUNT_BLOCKDEV $BENCHMARK_DBENCH_MOUNT_POINT
}


