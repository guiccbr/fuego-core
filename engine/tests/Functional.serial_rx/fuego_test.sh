#FUEGO_DEBUG=1

# Make sure that:
#
# -"getty" is not running on the DUTs port under test.
# -You have the HOST's serial device forwarded to docker (using a privileged
#   container created through "docker-create-usb-privileged-container.sh" works)
# -The jenkins user is on the dialout group:
#   > "sudo usermod -a -G dialout jenkins",
#   you may need to restart the container (logout/login) after adding the user
#   to the dialout group.

function noabort() {
	set +e
	eval "$@"
	RET=$?
	set -e
	return $RET
}

function test_build {
	true
}

function test_deploy {
	true
}

function test_run {
	assert_define FUNCTIONAL_SERIAL_RX_BAUDRATES
	assert_define FUNCTIONAL_SERIAL_RX_DUT_SERIAL_DEV
	assert_define FUNCTIONAL_SERIAL_RX_HOST_SERIAL_DEV

	local HOST_DEV=$FUNCTIONAL_SERIAL_RX_HOST_SERIAL_DEV
	local DUT_DEV=$FUNCTIONAL_SERIAL_RX_DUT_SERIAL_DEV
	local SENDATA="This is a test ASCII string"

	echo -n "$SENDATA" > /tmp/expected
	put /tmp/expected /tmp

	for RATE in $FUNCTIONAL_SERIAL_RX_BAUDRATES ; do
		stty -F $HOST_DEV $RATE raw -echo -echoe -echok
		# Receive for either SENDATA.length or 10 sec.
		cmd "stty -F $DUT_DEV $RATE raw -echo -echoe -echok min ${#SENDATA} time 100"
		cmd "nohup cat $DUT_DEV > /tmp/received &"
		# cmd seems to be returning slightly before the cat starts reading
		sleep 0.3
		echo -n "$SENDATA" > $FUNCTIONAL_SERIAL_RX_HOST_SERIAL_DEV
		# kill "cat" in case that the communication failed.
		noabort "cmd \"sync ; killall cat\""
		report_append "diff /tmp/received /tmp/expected && echo TEST-$RATE OK || echo TEST-$RATE FAILED"
	done
}

function test_processing {
	log_compare "$TESTDIR" "$(echo "$FUNCTIONAL_SERIAL_RX_BAUDRATES" | wc -w)" "^TEST.*OK" "p"
}

. $FUEGO_CORE/engine/scripts/functional.sh
