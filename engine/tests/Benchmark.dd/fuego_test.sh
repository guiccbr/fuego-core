# NOTES:
#  * time might not be a standalone program (it can be a shell builtin)
#    * can't use is_on_target to detect it
#  * busybox builtin time doesn't produce 'real' line
#  * autodetect of block device doesn't work for nfs-mounted root filesystems
#
function test_pre_check {
    is_on_target_path dd PROGRAM_DD
    assert_define PROGRAM_DD "Missing 'dd' program on target"
    # FIXTHIS - need to handle time as shell builtin case here
    #is_on_target_path time PROGRAM_TIME
    #assert_define PROGRAM_TIME "Missing 'time' program on target"
}

function test_run {
    if [ -z "$BENCHMARK_DD_BLOCK_DEVICE" ] ; then
        # if no block device specified, use root fs block device
        ROOT_MOUNT_STR=$(cmd "mount | grep \" on / \"")
        # fancy way to get the first string in ROOT_MOUNT_STR
        BENCHMARK_DD_BLOCK_DEVICE=$(IFS=" "; set -- ${ROOT_MOUNT_STR} ; echo $1)
    fi
    report "time dd if=$BENCHMARK_DD_BLOCK_DEVICE of=/dev/null bs=1000000 count=1024"
}
