function test_build {
    # ft2demos (Freetype2 Demos) depends on freetype
    tar jxvf $TEST_HOME/freetype-2.3.6.tar.bz2
    cd freetype-2.3.6/
    # get updates config.sub and config.guess files, so configure
    # doesn't reject new toolchains
    cp /usr/share/misc/config.{sub,guess} builds/unix
    ./configure --build=`uname -m`-linux-gnu --host=$HOST $OPTIONS CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" --target="$HOST" || build_error "error while configuring freetype"
    make || build_error "error while building freetype"

    # build ft2demos
    cd ..
    tar jxvf $TEST_HOME/ft2demos-2.3.6.tar.bz2
    cd ft2demos-2.3.6/
    patch -N -s -p0 < $TEST_HOME/ft2demos.Makefile.patch || build_error "error while patching ft2demos"
    make TOP_DIR=../freetype-2.3.6 || build_error "error while building ft2demos"
}

function test_deploy {
    put ft2demos-2.3.6/bin/.libs/*  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; find /usr/share/fonts/truetype/ -iname '*.ttf' | xargs -n1 ./ftdump $1"
}

function test_processing {
    local count=$(find /usr/share/fonts/truetype/ -iname '*.ttf' | wc -l)
    log_compare "$TESTDIR" "$count" ".*family:" "p"
    log_compare "$TESTDIR" "0" "fail|error|FAIL|ERROR" "n"
}
